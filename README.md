## Payment tracker

### Description
App for tracking payments.
Every minute it prints current balance to the standard output.
During runtime, you can add new payment in terminal: `USD 1000`

### Running

Run without input file: `gradle run -q`

Run with input file (replace with your path): `gradle run -q -PappArgs="['/Users/mawek/dev/workspace/personal/payment-tracker/src/main/resources/payments.txt']"`

if you don't have gradle installed, you can use gradle wrapper instead (just replace gradle with `./gradlew`)

### Testing

Run tests with `gradle test`