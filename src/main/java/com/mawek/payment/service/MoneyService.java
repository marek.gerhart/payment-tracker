package com.mawek.payment.service;


import com.mawek.payment.dto.Money;

import java.math.BigDecimal;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import static java.math.BigDecimal.ZERO;
import static java.util.Collections.unmodifiableSet;
import static org.apache.commons.lang3.Validate.notNull;

/**
 * Service for handling payments.
 * It uses private (non-persistent) storage for payments.
 * <p>
 * This implementation is thread safe.
 */
public final class MoneyService {

    private ConcurrentMap<String, Money> storage = new ConcurrentHashMap<>();

    /**
     * Return current balance. Ignore zero-valued records.
     */
    public Set<Money> findAll() {
        return unmodifiableSet(storage.values().stream().filter(m -> !m.getAmount().equals(ZERO)).collect(Collectors.toSet()));
    }

    /**
     * Add new payment using string. String is converted to {@link Money}. If conversion fails,
     * payment is ignored - no exception is thrown.
     * <p>
     * Check {@link Money#valueOf(String)} for string format info.
     *
     * @return whether payment was successfully processed
     */
    public boolean addPayment(String paymentStr) {
        try {
            return addPayment(Money.valueOf(paymentStr));
        } catch (Exception e) {
            // log exception ?
            return false;
        }
    }

    /**
     * Add new payments.
     *
     * @return whether payments were successfully processed
     */
    public boolean addPayment(Money... payments) {
        notNull(payments, "payments can't be null");

        for (Money money : payments) {
            storage.compute(money.getCurrency(), (currency, oldMoney) -> {
                final BigDecimal newAmount = oldMoney != null ? oldMoney.getAmount().add(money.getAmount()) : money.getAmount();
                return new Money(newAmount, money.getCurrency());
            });
        }

        return true;
    }

    // clearing for tests
    void clear() {
        storage.clear();
    }
}
