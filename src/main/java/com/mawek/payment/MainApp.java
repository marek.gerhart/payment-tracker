package com.mawek.payment;

import com.mawek.payment.service.MoneyService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static java.lang.System.out;

// main entry point of app, see readme for more info about params
public class MainApp {

    public static void main(String[] args) {
        final MoneyService moneyService = new MoneyService();

        if (args.length == 1) {
            try {
                Files.lines(Paths.get(args[0])).filter(l -> !l.trim().isEmpty()).forEach(moneyService::addPayment);
            } catch (IOException e) {
                out.print(format("Unable to read payments from %s. Detail: %s: ", args[0], e.toString()));
                System.exit(1);
            }
        }

        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(() -> out.println(format("Balance: %s", moneyService.findAll())), 1, 1, TimeUnit.MINUTES);

        final Scanner scanInput = new Scanner(System.in);

        while (true) {

            out.print("\nEnter payment: ");
            String input = scanInput.nextLine();

            if ("quit".equalsIgnoreCase(input)) {
                break;
            }

            out.println(moneyService.addPayment(input) ? "payment added" : "payment ignored");
        }

        scheduler.shutdownNow();
    }
}
