package com.mawek.payment.dto;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static org.apache.commons.lang3.Validate.notEmpty;
import static org.apache.commons.lang3.Validate.notNull;

/**
 * Immutable money DTO for storing amount and currency tuple.
 * */
public final class Money {

    private static final Pattern PATTERN = Pattern.compile("([a-z]{3})\\s+(-?[0-9]+)", CASE_INSENSITIVE);

    private final BigDecimal amount;
    private final String currency;

    public Money(BigDecimal amount, String currency) {
        notNull(amount, "amount can't be null");
        notEmpty(currency, "currency can't be empty");

        this.amount = amount;
        this.currency = currency.trim().toUpperCase();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    /**
     * Convert passed string to {@link Money}.
     * @throws IllegalArgumentException if passed argument format is invalid (empty or non-parseable)
     *
     * @return representative Money
     * */
    public static Money valueOf(String money) {
        notEmpty(money, "input can't be empty");

        final Matcher matcher = PATTERN.matcher(money.trim());
        if (!matcher.matches()) {
            throw new IllegalArgumentException(String.format("Invalid format of payment: %s. Input must match regular: %s", money, PATTERN.toString()));
        }

        return new Money(new BigDecimal(matcher.group(2)), matcher.group(1));
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                ", currency='" + currency + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Money)) {
            return false;
        }

        Money money = (Money) o;
        return Objects.equals(amount, money.amount) && Objects.equals(currency, money.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, currency);
    }
}
