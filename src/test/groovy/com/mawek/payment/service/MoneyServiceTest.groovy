package com.mawek.payment.service

import com.mawek.payment.dto.Money
import org.junit.Before
import org.junit.Test

import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

import static java.math.BigDecimal.valueOf

class MoneyServiceTest {

    MoneyService moneyService = new MoneyService()

    @Before
    void setup() {
        moneyService.clear()
    }

    @Test
    void testAddPaymentSimple() {
        moneyService.addPayment(new Money(valueOf(100), "CZK"))
        moneyService.addPayment(new Money(valueOf(200), "CZK"))
        moneyService.addPayment(new Money(valueOf(-20), "CZK"))
        moneyService.addPayment(new Money(valueOf(50), "usd"))

        Set<Money> moneySet = moneyService.findAll()

        assert moneySet.size() == 2
        assert moneySet.contains(new Money(valueOf(280), "CZK"))
        assert moneySet.contains(new Money(valueOf(50), "USD"))
    }

    @Test
    void testFindAllZero() {
        moneyService.addPayment(new Money(valueOf(100), "CZK"))
        moneyService.addPayment(new Money(valueOf(-100), "CZK"))
        moneyService.addPayment(new Money(valueOf(50), "usd"))

        Set<Money> moneySet = moneyService.findAll()

        assert moneySet.size() == 1
        assert moneySet.contains(new Money(valueOf(50), "USD"))
    }

    @Test
    void testAddPaymentParallel() {

        def money1 = [
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK")
        ]

        def money2 = [
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK")
        ]

        def money3 = [
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "CZK")
        ]

        ExecutorService executor = Executors.newWorkStealingPool()
        try {

            List<Future<Void>> futures = [executor.submit({ moneyService.addPayment(money1 as Money[]) } as Callable)]

            futures += executor.invokeAll(money2.collect { m -> return { moneyService.addPayment(m) } as Callable })
            futures += executor.invokeAll(money3.collect { m -> return { moneyService.addPayment(m) } as Callable })

            futures.each { it.get() }

            Set<Money> moneySet = moneyService.findAll()

            assert moneySet.size() == 1
            assert moneySet.contains(new Money(valueOf(3000), "CZK"))

            executor.shutdown()
        } finally {
            executor.shutdownNow()
        }
    }

    @Test
    void testAddPaymentParallelMultiCurrency() {

        def money1 = [
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "USD"),
                new Money(valueOf(100), "SKK"),
                new Money(valueOf(100), "EUR"),
                new Money(valueOf(100), "EUR"),
                new Money(valueOf(100), "CZK")
        ]

        def money2 = [
                new Money(valueOf(-100), "CZK"),
                new Money(valueOf(-100), "USD"),
                new Money(valueOf(-100), "SKK"),
                new Money(valueOf(-100), "EUR"),
                new Money(valueOf(-100), "EUR"),
                new Money(valueOf(-100), "CZK")
        ]

        def money3 = [
                new Money(valueOf(100), "CZK"),
                new Money(valueOf(100), "USD"),
                new Money(valueOf(100), "SKK"),
                new Money(valueOf(100), "EUR"),
                new Money(valueOf(100), "EUR"),
                new Money(valueOf(100), "CZK")
        ]

        ExecutorService executor = Executors.newWorkStealingPool()
        try {

            List<Future<Void>> futures = [executor.submit({ moneyService.addPayment(money1 as Money[]) } as Callable)]

            futures += executor.invokeAll(money2.collect { m -> return { moneyService.addPayment(m) } as Callable })
            futures += executor.invokeAll(money3.collect { m -> return { moneyService.addPayment(m) } as Callable })

            futures.each {
                it.get()
            }

            Set<Money> moneySet = moneyService.findAll()

            assert moneySet.size() == 4
            assert moneySet.contains(new Money(valueOf(200), "CZK"))
            assert moneySet.contains(new Money(valueOf(100), "USD"))
            assert moneySet.contains(new Money(valueOf(100), "SKK"))
            assert moneySet.contains(new Money(valueOf(200), "EUR"))

            executor.shutdown()
        } finally {
            executor.shutdownNow()
        }
    }

}