package com.mawek.payment.dto

import org.junit.Test

import static java.math.BigDecimal.valueOf

class MoneyTest {

    @Test
    void valueOf() {
        assert Money.valueOf("USD 1000") == new Money(valueOf(1000), "USD")
        assert Money.valueOf("usd 1000") == new Money(valueOf(1000), "USD")
        assert Money.valueOf("USD -1000") == new Money(valueOf(-1000), "USD")
        assert Money.valueOf("USD    1000") == new Money(valueOf(1000), "USD")
        assert Money.valueOf("  USD 1000 ") == new Money(valueOf(1000), "USD")
    }

}